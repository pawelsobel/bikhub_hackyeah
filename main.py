from mapa import Map
import pickle
from interpolator import Interpolator


with open(f"city_map_{2018}.pkl", "rb") as file:
    city_map = pickle.load(file)

interpolated = Interpolator.interpolate(city_map)
print(1)
